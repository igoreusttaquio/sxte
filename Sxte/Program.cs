﻿using System;
using Sxte.Modelos;
using Sxte.Modelos.Interfaces;
using System.IO;
using System.Collections.Generic;

namespace Sxte
{
    enum UF
    {
        AC, AL, AM, AP, BA, CE, DF, ES, GO, MA, MG, MS, MT,
        PA, PB, PE, PI, PR, RJ, RN, RO, RR, RS, SC, SE, SP, TO
    }

    enum Cod
    {
        x01, x02, x03, x04,
        a012, b01, b02, c893, m453
    }

    class Program
    {
        static void Main(string[] args)
        {

            var k99 = new Composicao { Uf = UF.AC.ToString(), Descricao = "EPI", Codigo = "k99"};
            k99.Insumos = new List<IInsumo>() {
                new Insumo { Codigo = Cod.x01.ToString(), Descricao = "Capacete", Quantidade = 0.17},
                new Insumo { Codigo = Cod.x02.ToString(), Descricao = "Protetor Solar Frasco 1L", Quantidade = 1},
                new Insumo { Codigo = Cod.x03.ToString(), Descricao = "Botina", Quantidade = 0.34},
                new Insumo { Codigo = Cod.x04.ToString(), Descricao = "Uniforme", Quantidade = 0.17}
            };

            var p012 = new Composicao { Uf = UF.AC.ToString(), Descricao = "Pedreiro com encargos", Codigo = "p012" };
            p012.Insumos = new List<IInsumo>() {
                new Insumo {Codigo = Cod.a012.ToString(), Descricao = "Pedreiro Mensalista", Quantidade = 0.0052},
                new Insumo {Codigo = Cod.b01.ToString(), Descricao = "Alimentação", Quantidade = 20 },
                new Insumo {Codigo = Cod.b02.ToString(), Descricao = "Transposrte", Quantidade = 40 },
                new Insumo {Composicao = k99,  Codigo = k99.Codigo, Descricao = k99.Descricao, Quantidade = 1}
            };

            var s1415 = new Composicao { Uf = UF.AC.ToString(), Descricao = "Muro com blocos de 12cm, sem acabamento", Codigo = "s1415" };
            s1415.Insumos = new List<IInsumo>()
            {
                new Insumo {Composicao = p012, Codigo = p012.Codigo, Descricao = "Pedreiro com encargos", Quantidade = 0.5},
                new Insumo {Codigo = Cod.c893.ToString(), Descricao = "Argamassa preparo manual", Quantidade = 12},
                new Insumo {Codigo = Cod.m453.ToString(), Descricao = "Blocos/Lajotas 12cm", Quantidade = 20}
            };


            CalcularComposicao(s1415);
        }

        static void CalcularComposicao(IComposicao composicao)
        {
            double total = 0;

            composicao.Insumos.ForEach(delegate (IInsumo insumo)
            {
                insumo = SetValorInsumo(insumo, composicao);
                if (insumo.Composicao != null)
                {
                    insumo.Composicao.Insumos.ForEach(delegate(IInsumo insumo1) {

                        insumo1 = SetValorInsumo(insumo1, insumo.Composicao);
                    
                    });
                }

            });

            total += composicao.CalcInsumos();

            Console.WriteLine($"O valor da composição {composicao.Descricao}, é: {total}");
            
        }

        static IInsumo SetValorInsumo(IInsumo insumo, IComposicao composicao)
        {
            TabelaValorIsumos().ForEach(delegate (ValorInsumo valorInsumo) {
                if (insumo.Codigo.Equals(valorInsumo.Codigo) && composicao.Uf.Equals(valorInsumo.Uf))
                {
                    insumo.Valor = valorInsumo.Valor;
                }
            });

            return insumo;
        }

        static List<ValorInsumo> TabelaValorIsumos()
        {
            List<ValorInsumo> valorInsumos = new List<ValorInsumo> ();
            string path = Path.Combine(Directory.GetCurrentDirectory(), "\\Data\\precosInsumos.csv");
            try
            {
                string[] lines = File.ReadAllLines(path);
                foreach (string line in lines)
                {
                    string[] columns = line.Split(';');
                    string codigo = columns[0];
                    string uf = columns[1];
                    string preco = columns[2];

                     valorInsumos.Add( new ValorInsumo
                     {
                        Codigo = codigo,
                        Uf = uf,
                        Valor = double.Parse(preco)
                     });

                }

            }
            catch(Exception)
            {   
                Console.WriteLine($"Arquivo precosInsumos.csv não encontrado no caminho: {path}");
            }

            return valorInsumos;
        }
    }
}
