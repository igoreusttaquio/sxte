﻿namespace Sxte.Modelos
{
    public class ValorInsumo
    {
        public string Codigo { get; set; }
        public string Uf { get; set; }
        public double Valor { get; set; }
    }
}
