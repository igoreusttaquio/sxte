﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sxte.Modelos.Interfaces
{
    interface IInsumo
    {
        string Codigo { get; set; }
        string Descricao { get; set; }
        double Valor { get; set; }
        double Quantidade { get; set; }

        public abstract double CalcValor();
        public IComposicao Composicao { get; set; }
    }
}
