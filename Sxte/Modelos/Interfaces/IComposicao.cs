﻿using System.Collections.Generic;

namespace Sxte.Modelos.Interfaces
{
    interface IComposicao
    {
        string Codigo { get; set; }
        public string Descricao { get; set; }
        public string Uf { get; set; }
        List<IInsumo> Insumos { get; set; }

        public abstract double CalcInsumos();
    }
}
