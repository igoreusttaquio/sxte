﻿using Sxte.Modelos.Interfaces;

namespace Sxte.Modelos
{
    class Insumo : IInsumo
    {
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public double Valor { get; set; }
        public double Quantidade { get; set; }

        public IComposicao Composicao { get; set; }

        public double CalcValor()
        {
            double total = Valor * Quantidade;
            
            if(Composicao!= null)
            {
                total += Composicao.CalcInsumos() * Quantidade;
            }

            return total;
        }
    }
}
