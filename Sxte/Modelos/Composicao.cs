﻿using System.Collections.Generic;
using Sxte.Modelos.Interfaces;

namespace Sxte.Modelos
{
    class Composicao : IComposicao
    {
        public string Uf { get; set; }
        public string Codigo { get; set; }
        public string Descricao { get; set; }
        public List<IInsumo> Insumos { get; set; }

        public double CalcInsumos()
        {
            double total = 0;
            Insumos.ForEach(delegate (IInsumo insumo) {
                total += (insumo.CalcValor());
            });

            return total;
        }
    }
}
